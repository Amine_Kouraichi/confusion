import React, {Component} from 'react';
import { Modal,ModalBody,Col,ModalHeader,Label,Row ,Breadcrumb,BreadcrumbItem,Card,CardImg, CardImgOverlay,CardText,CardBody,CardTitle,Button } from 'reactstrap';
import {Link} from 'react-router-dom';
import { Control, LocalForm, Errors} from 'react-redux-form';
import {Form,FormGroup,Input} from 'reactstrap';
import {Loading} from './LoadingComponent';
import {baseUrl} from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const maxLength=(len)=>(val)=> !(val) || (val.length<=len);
const minLength=(len)=>(val)=>  (val) && (val.length>=len);

    
    class CommentForm extends Component{
        constructor(props){
            super(props);
            this.state={
            isModalOpen:false,
        };
        this.toggleModal=this.toggleModal.bind(this);
        this.handleComment=this.handleComment.bind(this);
    }

    toggleModal(){
        this.setState({isModalOpen: !this.state.isModalOpen});

    }

    handleComment(values){
            this.toggleModal();
            this.props.postComment(this.props.dishId,values.rating,values.author,values.comment);
    }
        
        render(){
            return(
                <div>
                <Button outline onClick={this.toggleModal}>
                    <span className="fa fa-comment fa-lg"></span> Submit Comment
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader isOpen={this.state.isModalOpen} toggle={this.toggleModal}>Submit your comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values)=>this.handleComment(values)}>

                            <FormGroup>
                                
                                 <Label htmlFor="rating" md={2}><strong>Rating</strong></Label>

                                 <Col md={10}>
                                    <Control.select model=".rating" name="rating"
                                        className= "browser-default custom-select">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                               
                            </FormGroup>

                            <FormGroup>
                                <Label htmlFor="author" md={5}><strong>Your Name</strong></Label>
                                <Col md={10}>
                                    <Control.text model=".author" id="author" name="author"
                                        placeholder="Your Name"
                                        className="form-control"
                                        validators={{
                                            minLength:minLength(3),maxLength:maxLength(15)
                                        }}
                                        />
                                    <Errors
                                        className="text-danger"
                                        model=".author"
                                        show="touched"
                                        messages={{
                                            minLength: 'Must be greater than 2 charachters',
                                            maxLength: 'Must be 15 charachters or less'
                                        }}
                                        />
                                    </Col>
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="comment" md={2}> <strong> Comment </strong></Label>
                                <Col md={10}>
                                    <Control.textarea model=".comment" id="comment" name="comment"
                                         rows="12"
                                         className="form-control"/>
                                    
                                </Col>
                             </FormGroup>   
                         <FormGroup>
                          <Col md={{size: 10, offset: 0}}>
                                <Button type="submit" value="submit" className="bg-primary">Submit</Button>
                          </Col>        
                            </FormGroup>


                        </LocalForm>
                    </ModalBody>
                </Modal>    
                </div>

                        );
        }
    }


	function RenderDish({dish}){
        	return(
                    <FadeTransform in 
                        transformProps={{
                        existTransform: 'scale(0.5) translateY(-50%)'
                     }}>
					<Card  >
						<CardImg width="100%" src={baseUrl +'/'+ dish.image} alt={dish.name} />
                    	<CardBody>
                     	<CardTitle>{dish.name}</CardTitle>
                     	<CardText>{dish.description}</CardText>
                    	</CardBody>
					</Card>
					</FadeTransform>
			);
       	 }
	function RenderComments({comments,postComment,dishId}){

		const selectedComment=comments.map((comment)=>{
			return(
                   
					<ul class = "list-unstyled">
                    <Stagger in>
                    <Fade in>
						<li key={comment.id}>
                        <p>{comment.comment}</p>
						 <p>-- {comment.author},
                    &nbsp;
                    {new Intl.DateTimeFormat('en-US', {
                            year: 'numeric',
                            month: 'long',
                            day: '2-digit'
                        }).format(new Date(comment.date))}
                    </p></li>
                    </Fade>
                    </Stagger>
					</ul>
					
                   
				);
		});

		return(
				
					<div className="container">
						<div className="row">
                        <Stagger in>
						{selectedComment}
                        </Stagger>
						</div>
						<div className="row">
						<CommentForm dishId={dishId} postComment={postComment}/>
						</div>
					</div>
				
			);
		
	}


	
		const DishDetail=(props)=>{
            if(props.isLoading){
                return(
                        <div className="container">
                            <div className="Row">
                                <Loading/>
                            </div>
                        </div>
                    );
            }else if (props.errMess){
                return(
                        <div className="container">
                            <div className="Row">
                                <h4>{props.errMess}</h4>
                            </div>
                        </div>
                    );
            }
			else if (props.dish!=null){

        return(
        		<div className="container">
        			<div className="row">
						<Breadcrumb>
							<BreadcrumbItem> <Link to='/menu'>Menu</Link></BreadcrumbItem>
							<BreadcrumbItem active> {props.dish.name}</BreadcrumbItem>
						</Breadcrumb>
						<div className="col-12">
							<h3>{props.dish.name}</h3>
							<hr/>
						</div>
					</div>
        			<div className="row">
        				<div className="col-12 col-md-5 m-1 col-xs-12">
        				<RenderDish dish={props.dish} />
        				</div>
        				<div className="col-12 col-md-5 m-1 col-xs-12">
        				 <h4> Comments </h4>
                         
						<RenderComments comments={props.comments}
                                        postComment={props.postComment}
                                        dishId={props.dish.id} />

						</div>
				</div>	
				</div>
        	)

        } else{
        	return<div></div>
        }

	}




export default DishDetail;